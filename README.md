# README

Kultureinrichtungen stehen vor der Herausforderung ihren Nutzer*innen reichhaltige Daten zu den von ihnen verwalteten Objekten anzubieten, um Forscher\*innen Datenanalysen auf dem von ihnen kuratierten Wissensbestand zu ermöglichen. 
Die Sammlung dieser Daten ist aufwändig und zudem durch die zugrundeliegenden CMS- und Datenbanksysteme limitiert. Aus diesem Grund können einige Informationen nicht erfasst und aggregiert dargestellt werden. Zu diesen, für prosopografische Fragestellungen interessanten, Informationen gehören am Beispiel des Lehrkörpers der Freien Universität beispielsweise Angaben zu Berufen, Wirkungsorten und Auszeichnungen einzelner Personen.
In der Folge bleiben die Wissensbestände unvollständig und müssen durch Nutzer\*innen manuell und unter der Nutzung verschiedener Quellen und Werkzeuge zusammengeführt werden, bevor daraus neue Erkenntnisse gewinnbar sind. Dieser Prozess der Anreicherung (Enrichment) ist in der Abbildung dargestellt:

![enrichment.png](enrichment.png)

Diese Anreicherung kann manuell erfolgen, z. B. mit Hilfe von [Open Refine](https://openrefine.org/) wie [von den Kollegen des HBZ demonstriert](https://blog.lobid.org/2018/08/27/openrefine.html) ([siehe auch Tutorial von der GNDcon2021](https://slides.lobid.org/2021-gndcon-reconcile/#/)). Das Vorgehen hat Stärken für die Zuordnung von Identifikationsnummern zu den Datensätzen (Reconciliation), eignet sich jedoch nicht für die regelmäßige Aktualisierung der mit den Identifikatoren abrufbaren Informationen.
An dieser Stelle soll daher eine Methode untersucht werden mit der Listen von GND-Nummern (hier zu Personen der Freien Universität) durch zusätzliche Informationen aus Wikidata und vergleichbaren offenen Datenquellen angereichert und regelmäßig aktualisiert werden können. 
In diesem [Demonstrator](script.ipynb) wird gezeigt wie sich Datenintegrationsprozesse mit einfachen Mitteln automatisieren lassen und hierzu Werkzeuge aus dem Bereich von [Linked Data](https://de.wikipedia.org/wiki/Linked_Open_Data) eingesetzt. 

Zu Demonstrationszwecken wird hier eine Datensammlung von Professor*innen der Freien Universität aus Wikidata erstellt, weil zu diesem Zeitpunkt noch keine frei lizenzierte Datensammlung der FU Professor*innen veröffentlicht ist. Die resultierende CSV-Datei wird unter Nutzung von [SPARQL-Abfragen](https://de.wikipedia.org/wiki/SPARQL) mit den Kommandozeilen-Werkzeugen [JENA](https://jena.apache.org/documentation/tools/index.html) und [tarql](https://tarql.github.io/) angereichert, wiederum unter Nutzung des [Wikidata Query Service](https://query.wikidata.org/). Als Eingabe- und Ausgabeformat wurde CSV gewählt, weil die meisten Datenbanksysteme in der Lage sind dieses Format zu exportieren und zu importieren, sodass eine [regelmäßige Synchonisation](https://de.wikipedia.org/wiki/Cron) mit externen Datenquellen erfolgen kann. Das zugrundeliegende Skript kann in einem [Jupyter Notebook via mybinder.org](https://mybinder.org/v2/gl/webvstdlch%2Fenrichment/HEAD) ausprobiert und angepasst werden. Neben der Verwendung der Testdatensammlung können auch eigene CSV-Daten angereichert werden, wenn Sie über global eindeutige Identifier (etwa eine GND-Kennung oder Wikidata ID) verfügen (siehe Abschnitt Benutzung unten). 

## Quick Start:
Wer einen ersten Eindruck gewinnen möchte kann den Demonstrator über [mybinder.org](https://mybinder.org/) starten. Da hierbei ein eigener Docker Container aus den Daten des Repository erzeugt wird, kann diese virtuelle Umgebung wird NICHT für den PRODUKTIVBETRIEB oder die Weiterentwicklung empfohlen werden! Der Start des Demonstrators erfolgt über den Klick auf das Binder Icon 👉 [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/webvstdlch%2Fenrichment/HEAD).


## Installation:
Für die Verwendung des in [script.ipynb](script.ipynb) dargestellten Ablauf wird JAVA benötigt. Die Skripte benötigen die Kommandozeilentools von [JENA](https://jena.apache.org/) sowie [tarql](https://tarql.github.io/) benötigt. Siehe auch die einbindung dieser Bibliotheken in der Datei `.bash_aliases`.

Als Entwicklungsumgebung wird [Visual Studio](https://visualstudio.microsoft.com/de/) mit folgenden den Plugins empfohlen:
- [Jupyter Extension for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=ms-toolsai.jupyter)
- [SPARQL Language Server (with Stardog extensions)](https://marketplace.visualstudio.com/items?itemName=stardog-union.vscode-langserver-sparql)
- [sparql-auto-completion](https://marketplace.visualstudio.com/items?itemName=Fireblossom.sparql-auto-completion)
- [Stardog RDF Grammars](https://marketplace.visualstudio.com/items?itemName=stardog-union.stardog-rdf-grammars)

## Benutzung

Zur Nutzung eigener CSV-Daten müssen diese über eine GND Nummer, eine ID des Ursprungssystems und einen Namen verfügen und im Verzeichnis `data` mit einem Kürzel des Datensets abgelegt werden (z. B. dataLCA.csv). Die Transformation zu RDF wird in der SPARQL Konfigurationsdatei `./query/csv2rdf.rq` gesteuert, wo ggf. die Spaltennamen anzupassen sind. Die Transformation wird im [script.ipynb](script.ipynb), oder mit den dort hinterlegten Kommandos auf der Shell gestartet.

### Hinweis zu den verwendeten Daten

Zur Erstellung einer Testdatensammlung dient dient hier die SPARQL Abfrage [getTestData.rq](./query/getTestData.rq) im Verzeichnis `query`. Das folgende Kommando erstellt diese Testdatensammlung `dataAll.csv` im Verzeichnis `data`.

```bash
rsparql --query query/getTestData.rq --service=https://query.wikidata.org/sparql --results=CSV > data/data.csv
```  
  
Es empfielt sich für die Entwicklung einen Auszug aus dem Datenset zu verwenden, da die Abfrage je nach Umfang der Daten und Komplexität der SPARQL-Anfragen (siehe Verzeichnis `query` entsprechend lange dauern kann. Ein Test mit ca. 2000 Personendatensätzen des Universitätsarchivs der Freien Universität dauert bspw. 20 Minuten.
Zu Testzwecken wird ein Auszug der ersten 9 Datensätze erstellt.

```bash
head -n 10 data/data.csv > data/dataTest.csv
```  

 ### Übersicht der Verzeichnisse 
```language-bash
<>
|_ .binder/ <= Konfigurationsdateien für die Demo via Binder
|_ data/ <= CSV Dateien zur Anreicherung
|_ query/ <= SPARQL Abfragen für die Schritte 1-4
|_ results/ <= Angereicherte CSV
|_ tmp/ <= RDF Arbeitsdateien als .ttl (Turtle Syntax)
|_ .bash_aliases <= macht JENA und tarql auf der Shell verfügbar (von mybinder benötigt) 
|_ script.ipynb <= Jupyter Notebook zur Dokumentation / Testen
```  
 
## Ergebnisse 

Aktuell werden folgende Informationen aus Wikidata in die Ergebnisdatei geschrieben, wobei jede Zeile eine Person repräsentiert. Bei mehreren Daten z. B. Arbeitsgebiete werden die Angaben innerhalb der Zelle mit einem Separator getrennt, z. B. "|":
- langSpokenOrWritten (Sprachen, in der die Person veröffentlicht hat)
- citizenships (Nationalität(en) der Person)
- birthYear und deathYear (Jahreszahl in der die Person geboren oder gestorben ist)
- birthPlace (Geburtsort)
- birthPlaceGeo (Geburtsort als Wikidata Entität mit Geokoordinaten)
- awards (Ehrungen, mit denen die Person ausgezeichnet wurde)
- fieldsOfWork (Arbeitsgebiete der Person)
- receivedEducationAt (Universitäten und Schulen an denen die Person ausgebildet wurde)
- receivedEducationAtGeo (wie oben aber als Wikidata-Entität mit Geo-Koordinaten in geschweiften Klammern)
- employers (Arbeitgeber der Person)
- employersGeo (Arbeitgeber der Person als Wikidata-Entität mit Geo-Koordinaten)
- picUrl (ein Link zu einer Bilddatei aus Wikipedia, falls verfügbar)

Die Verarbeitung der für den Demonstrator erstellten Testdatensammlung von 1084 Hochschullehrer*innen in Wikidata, die an der FU Berlin beschäftigt sind oder waren und über eine GND-ID verfügen (Stand 20. Oktober 2022) benötigt ca. 20 Minuten. Demgegenüber besitzt das Universitätsarchiv der Freien Universität eine Datensammlung von mehr als 4000 Personen, die dem Lehrkörper der FU zugeordnet werden können (Zeitraum 1948–2012). Derzeit besitzen ca. 65 % der Datensätze des FU-Archivs eine GND-Nummer, Tendenz steigend. Zusätzliche Angaben zu Lebensdaten, Geburts- und Sterbeorten, Berufen, Tätigkeitsorten und Auszeichnungen können somit automatisiert und mit einem vertretbaren Zeitaufwand angereichert werden. Für zukünftige Arbeiten wäre es außerdem wünschenswert Daten die im Archiv vorliegenden Informationen zu Wikidata zu bringen, um deren Sichtbarkeit und Nachnutzbarkeit für die Wissenschaftscommunity zu erhöhen.

## Ideen zum Weitermachen
  
- Analysen zur Feldbelegung durchführen, z. B. für wie viele Personen sind Auszeichnungen vorhanden, bei wie vielen liegen Lebensdaten vor, wie vollständig sind Angaben zu Geburts- und Sterbeorten/-daten, usw.
- Einzelne Fragestellungen beantworten, z. B.:
  - Welches sind die häufigsten Berufe, neben der Tätigkeit als Hochschullehrer*in?
  - An welchen Hochschulen haben die meisten Personen (neben der FU Berlin) studiert oder gearbeitet?
  - Welche Auszeichnungen wurden am häufigsten verliehen? Wann? Welche Personen haben die meisten Ehrungen erhalten?
- Weitere Quellen für die Anreicherung finden/nutzen (z. B. <https://lobid.org/>)
- Informationsvisualisierungen erstellen, z. B. Geburts- und Sterbeorte, sowie Lebensstationen einzelner oder aller Personen auf einer Karte (z. B. [Roman Herzog](https://www.deutsche-biographie.de/pnd119155087.html) in der dt. Biographie)
- Eigene und geprüfte Daten, z. B. des FU Archivs in [QuickStatements](https://www.wikidata.org/wiki/Help:QuickStatements) konvertieren und in [Wikidata importieren](https://www.wikidata.org/wiki/Wikidata:Data_Import_Guide)
- Vergleich des Ansatz mit anderen Anreicherungsstrategien (z. B. via Javascript-Anwendung). Mögliche Untersuchungsfragen: Geschwindigkeit, Konfigurierbarkeit, Handhabbarkeit in nachfolgenden Prozessen, usw.


## Autor
Johannes Hercher (hercher@ub.fu-berlin.de)
 
## Dank
Dieses Projekt wurde im Rahmen eines [Hackathon an der Universitätsbibliothek der Freien Universität am 13.10.2022](https://wikis.fu-berlin.de/x/YAGkTQ) initiiert. Mein Dank geht an meine Kolleg*innen für die inspirierenden Gespräche und hilfreichen Tipps zur Umsetzung, insbesondere:  Michael Müller, Martin Hampl, Fabian Etling, Sven Weindel, Georg Kallidis und Alexander Fütterer.

## Lizenz 
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.


